FROM alpine:3.15 AS builder

RUN apk --no-cache add build-base

COPY ./app/hello.c /
RUN gcc hello.c -o hello

FROM python:3.10-alpine

ARG USER=pytime
ARG ID=2000

RUN addgroup -g ${ID} ${USER} && \
    adduser -D -H -G ${USER} -u ${ID} -h /nonexistent -s /sbin/nologin ${USER}

RUN apk --no-cache add iputils libcap bash

ADD ./app/requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt

COPY ./app /app/
COPY --from=builder /hello /app

EXPOSE 80

WORKDIR /app

RUN chmod 4755 debug

USER ${USER}

ENV FLASK_APP=pytime

ENTRYPOINT [ "flask", "run", "--host=0.0.0.0", "--port=80" ]
